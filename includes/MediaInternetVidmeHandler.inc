<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle Vidme video.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetVidmeHandler extends MediaInternetBaseHandler {

  /**
   *
   */
  public function get_item_code($embedCode) {
    $patterns = array(
      '@vid\.me/([0-9A-Za-z\@\&\$_-]+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1]) && !isset($matches[2])) {
        return ($matches[1]);
      }
    }
    return FALSE;
  }

  /**
   *
   */
  public function parse($embedCode) {
    if ($this->get_item_code($embedCode)) {
      return file_stream_wrapper_uri_normalize('vidme://v/' . $this->get_item_code($embedCode));
    }
  }

  /**
   *
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  /**
   *
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);
    // Try to default the file name to the video's title.
    if (empty($file->filename) && $info = $this->getOEmbed()) {
      $file->filename = truncate_utf8($info['video']['title'], 255);
    }
    return $file;
  }

  /**
   * Returns information about the media.
   *
   * See http://www.oembed.com.
   *
   * @return
   *   If oEmbed information is available, an array containing 'title', 'type',
   *   'url', and other information as specified by the oEmbed standard.
   *   Otherwise, NULL.
   */
  public function getOEmbed() {

    $uri = $this->parse($this->embedCode);
    $external_url = file_create_url($uri);

    $oembed_url = url('https://api.vid.me/videoByUrl', array('query' => array('url' => $external_url, 'format' => 'json')));
    $response = drupal_http_request($oembed_url);

    if (!isset($response->error)) {
      return drupal_json_decode($response->data);
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      return;
    }
  }

}
