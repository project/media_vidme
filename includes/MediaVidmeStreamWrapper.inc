<?php

/**
 * @file
 * Extends the MediaReadOnlyStreamWrapper class to handle Vidme video.
 */

/**
 * Create an instance like this:
 *  $soundcloud = new MediaSoundCloudStreamWrapper('vidme://v/[video-code]');.
 */
class MediaVidmeStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'https://vid.me/';
  protected $parameters = array('u', 'a', 'g', 's');

  /**
   *
   */
  static function getMimeType($uri, $mapping = NULL) {
    return 'video/vidme';
  }

  /**
   *
   */
  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    $uri = "vidme://v/" . $parts['v'];
    $external_url = file_create_url($uri);

    $oembed_url = url('https://api.vid.me/videoByUrl', array('query' => array('url' => $external_url, 'format' => 'json')));
    $response = drupal_http_request($oembed_url);

    if (!isset($response->error)) {
      $data = drupal_json_decode($response->data);
      return $data['video']['thumbnail_url'];
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      return;
    }
  }

  /**
   *
   */
  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-vidme/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }

  /**
   *
   */
  function interpolateUrl() {
    $url = "";
    if (isset($this->parameters['v'])) {
      $url = $this->base_url . check_plain($this->parameters['v']);
    }
    return $url;
  }

}
