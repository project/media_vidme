<?php

/**
 * @file
 * File formatters for SoundCloud audio.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_vidme_file_formatter_info() {
  $formatters['media_vidme_video'] = array(
    'label' => t('Vidme Video'),
    'file types' => array('video'),
    'default settings' => array(
      'width' => variable_get('media_vidme_width', 640),
      'height' => variable_get('media_vidme_height', 360),
      'autoplay' => variable_get('media_vidme_autoplay', ''),
      'fullscreen' => variable_get('media_vidme_fullscreen', ''),
      'muted' => variable_get('media_vidme_muted', ''),
      'repeat' => variable_get('media_vidme_repeat', ''),
    ),
    'view callback' => 'media_vidme_file_formatter_video_view',
    'settings callback' => 'media_vidme_file_formatter_video_settings',
  );
  $formatters['media_vidme_image'] = array(
    'label' => t('Vidme Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_vidme_file_formatter_image_view',
    'settings callback' => 'media_vidme_file_formatter_image_settings',
  );
  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_vidme_file_formatter_video_view($file, $display, $langcode) {

  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'vidme') {
    $element = array(
      '#theme' => 'media_vidme_video',
      '#uri' => $file->uri,
    );

    foreach (array('width', 'height', 'autoplay', 'fullscreen', 'repeat', 'muted') as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_vidme_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );
  $element['autoplay'] = array(
    '#title' => t('Autoplay'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autoplay'],
  );
  $element['fullscreen'] = array(
    '#title' => t('Fullscreen'),
    '#type' => 'checkbox',
    '#default_value' => $settings['fullscreen'],
  );
  $element['muted'] = array(
    '#title' => t('Muted'),
    '#type' => 'checkbox',
    '#default_value' => $settings['muted'],
  );
  $element['repeat'] = array(
    '#title' => t('Repeat'),
    '#type' => 'checkbox',
    '#default_value' => $settings['repeat'],
  );
  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_vidme_file_formatter_audio_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  // WYSIWYG does not yet support video inside a running editor instance.
  if ($scheme == 'vidme') {
    $element = array(
      '#theme' => 'media_vidme_video',
      '#uri' => $file->uri,
    );
    foreach (array('width', 'height', 'autoplay', 'fullscreen', 'repeat', 'muted') as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_vidme_file_formatter_audio_settings($form, &$form_state, $settings) {
  $element = array();
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );
  $element['autoplay'] = array(
    '#title' => t('Autoplay'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autoplay'],
  );
  $element['fullscreen'] = array(
    '#title' => t('Fullscreen'),
    '#type' => 'checkbox',
    '#default_value' => $settings['fullscreen'],
  );
  $element['muted'] = array(
    '#title' => t('Muted'),
    '#type' => 'checkbox',
    '#default_value' => $settings['muted'],
  );
  $element['repeat'] = array(
    '#title' => t('Repeat'),
    '#type' => 'checkbox',
    '#default_value' => $settings['repeat'],
  );
  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_vidme_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'vidme') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);

    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => $wrapper->getLocalThumbnailPath(),
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
      );
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_vidme_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();

  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );

  return $element;
}
