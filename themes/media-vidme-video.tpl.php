<?php

/**
 * @file Media_vidme/themes/media-vidme-video.tpl.php.
 *
 * Template file for theme('media_vidme_video').
 */
?>

<div class="<?php print $classes; ?> media-vidme-<?php print $id; ?>">
  <iframe class="media-vidme-player" <?php print $api_id_attribute; ?>width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" <?php if($fullscreen == 1) {print " allowfullscreen webkitallowfullscreen mozallowfullscreen ";}  ?> scrolling="no" ><?php print $alternative_content; ?></iframe>
</div>
