<?php

/**
 * @file Media_soundcloud/themes/media_soundcloud.theme.inc.
 *
 * Theme and preprocess functions for Media: SoundCloud.
 */

/**
 * Preprocess function for theme('media_soundcloud_audio').
 */
function media_vidme_preprocess_media_vidme_video(&$variables) {

  foreach (array('width', 'height', 'fullscreen') as $themevar) {
    $variables[$themevar] = $variables['options'][$themevar];
  }

  $uri = $variables['uri'];
  $external_url = file_create_url($uri);

  // Make the file object available.
  $file_object = file_uri_to_object($variables['uri']);

  $patterns = array(
    '@vid\.me/([0-9A-Za-z\@\&\$_-]+)@i',
  );
  $video_id = '';
  foreach ($patterns as $pattern) {
    preg_match($pattern, $external_url, $matches);
    if (isset($matches[1]) && !isset($matches[2])) {
      $video_id = $matches[1];
    }
  }
  if (isset($variables['options']['autoplay']) && ($variables['options']['autoplay'] == 1)) {
    $query['autoplay'] = 1;
  }
  if (isset($variables['options']['muted']) && ($variables['options']['muted'] == 1)) {
    $query['muted'] = 1;
  }
  if (isset($variables['options']['repeat']) && ($variables['options']['repeat'] == 1)) {
    $query['loop'] = 1;
  }

  $variables['api_id_attribute'] = 'id="' . $video_id . '" ';

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file_object->filename);
  // @TODO: Find an easy/ not too expensive way to get the vidme description
  // to use for the alternative content.
  $variables['alternative_content'] = t('Audio titled @title', array('@title' => $variables['title']));

  // Build the iframe URL with options query string.
  $vidme_url = 'https://vid.me/e/' . $video_id;
  $variables['url'] = url($vidme_url, array('query' => $query));

}
